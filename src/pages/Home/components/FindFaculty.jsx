import React from 'react';
import { Row, Col, Select, Button } from "antd";

const { Option } = Select;

export default () => {
    return (
        <Row gutter={[24, 8]} align="middle">
            <Col sm={24} md={24} lg={5}>
                <span className="section-header">
                    O'z <br /> yo'nalishingni <br />tanla
                </span>
            </Col>
            <Col xs={24} md={12} lg={5}>
                <CustomSelect />
            </Col>
            <Col xs={24} md={12} lg={5}>
                <CustomSelect />
            </Col>
            <Col xs={24} md={12} lg={5}>
                <CustomSelect />
            </Col>
            <Col xs={24} md={12} lg={4}>
                <Button className="custom-bordered-btn">
                    Qidirish
                </Button>
            </Col>
        </Row>
    );
}

const CustomSelect = () => {
    return (
        <Select
            showSearch
            className="custom-select"
            placeholder="Tanlang"
            optionFilterProp="children"
            // onChange={onChange}
            // onFocus={onFocus}
            // onBlur={onBlur}
            // onSearch={onSearch}
            filterOption={(input, option) =>
                option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
        >
            {
                data.map((obj) => <Option key={obj.id} value={obj.id}>{obj.label}</Option>)
            }
        </Select>
    )
}

const data = [
    {
        id: 1,
        label: "Bakalavr",
    },
    {
        id: 2,
        label: "Magistr",
    },
]