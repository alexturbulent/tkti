import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Layout, PageHeader, message } from 'antd';
import { withRouter } from "react-router-dom";

/* STYLES */
import "./dashboard.less";


import {
    MenuUnfoldOutlined,
    MenuFoldOutlined,
} from '@ant-design/icons';


/* COMPONENTS */
import LazyLoadErrorBoundary from "../../globalComponents/LazyLoadErrorBoundary";
import TopNavUserMenu from "./components/TopNavUserMenu";
import Sidebar from "./components/Sidebar";

import Home from './pages/Home/Home';
import Info from './pages/Info/Info';
import NotFound from '../NotFound/NotFound';

const { Content } = Layout;

class Dashboard extends React.Component {
    state = {
        collapsed: false,
    };

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        })
    }

    goToMenu = (key) => {
        console.log(key);

        if (key) {
            this.props.history.push(key);
        } else {
            message.info('Not found this kind of menu');
        }
    }

    render() {
        return (
            <Router>
                <Layout>

                    <Sidebar collapsed={this.state.collapsed} onClose={this.toggle} />

                    <Layout className="site-layout">
                        <PageHeader
                            ghost={false}
                            onBack={false}
                            title={
                                React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                                    className: 'trigger',
                                    onClick: this.toggle,
                                })
                            }
                            extra={[
                                <TopNavUserMenu key="userControlBtn" />
                            ]}
                        >
                        </PageHeader>

                        <Content
                            className="site-layout-background"
                            style={{
                                margin: '24px 16px',
                                padding: '24px',
                            }}
                        >
                            <LazyLoadErrorBoundary>
                                <Switch>
                                    <Route path='/dashboard/info' render={() => <Info />} />
                                    <Route path='/dashboard' render={() => <Home />} />
                                    <Route render={() => <NotFound />} />
                                </Switch>
                            </LazyLoadErrorBoundary>
                        </Content>
                    </Layout>
                </Layout>
            </Router>
        )
    }
}

export default withRouter(Dashboard);