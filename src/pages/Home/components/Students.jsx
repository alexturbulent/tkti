import React from 'react';
import { Row, Col, Card, Tabs, Typography, Button } from 'antd';

import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';

import student from "../../../assets/img/student.png";

const { TabPane } = Tabs;
const { Paragraph } = Typography;

const responsive = {
    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 5,
        slidesToSlide: 2 // optional, default to 1.
    },
    tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 2,
        slidesToSlide: 2 // optional, default to 1.
    },
    mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 1,
        slidesToSlide: 1 // optional, default to 1.
    }
}

const Students = () => {
    return (
        <React.Fragment>
            <div className="header-section">
                <div className="container py-3">
                    <h3 className="section-header">Iqtidorli talabalar</h3>
                </div>
            </div>
            <Row className="container" justify="end">
                <Tabs defaultActiveKey="1">
                    <TabPane className="py-2" tab="Bakalavr" key="1">
                        <Carousel
                            swipeable={true}
                            draggable={false}
                            responsive={responsive}
                            infinite={true}
                            keyBoardControl={true}
                            customTransition="all .5"
                            transitionDuration={500}
                            containerClass="carousel-container"
                            removeArrowOnDeviceType={["tablet", "mobile"]}
                            // itemClass="carousel-item-padding-40-px"
                        >
                            <Card
                                className="student-card"
                                cover={<img className="cover-img" alt="Img error" src={student} />}
                            >
                                <p className="student-name">Mirzaev Saidislom Abdug'ulom o'g'li</p>
                                <Paragraph className="description" ellipsis={{ rows: 5 }}>Universiada-2019 sport musobaqasida Dzyu-do bo‘yicha 3-o‘rin g‘olibi</Paragraph>
                            </Card>
                            <Card
                                className="student-card"
                                cover={<img className="cover-img" alt="Img error" src={student} />}
                            >
                                <p className="student-name">Mirzaev </p>
                                <Paragraph className="description" ellipsis={{ rows: 5 }}>Universiada-2019 sport musobaqasida Dzyu-do bo‘yicha 3-o‘rin g‘olibi</Paragraph>
                            </Card>
                            <Card
                                className="student-card"
                                cover={<img className="cover-img" alt="Img error" src={student} />}
                            >
                                <p className="student-name">Saidislom Abdug'ulom o'g'li</p>
                                <Paragraph className="description" ellipsis={{ rows: 5 }}>Universiada-2019 sport musobaqasida Dzyu-do bo‘yicha 3-o‘rin g‘olibi</Paragraph>
                            </Card>
                            <Card
                                className="student-card"
                                cover={<img className="cover-img" alt="Img error" src={student} />}
                            >
                                <p className="student-name">Abdug'ulom o'g'li</p>
                                <Paragraph className="description" ellipsis={{ rows: 5 }}>Universiada-2019 sport musobaqasida Dzyu-do bo‘yicha 3-o‘rin g‘olibi</Paragraph>
                            </Card>
                            <Card
                                className="student-card"
                                cover={<img className="cover-img" alt="Img error" src={student} />}
                            >
                                <p className="student-name">Mirzaev Saidislom Abdug'ulom o'g'li</p>
                                <Paragraph className="description" ellipsis={{ rows: 5 }}>Universiada-2019 sport musobaqasida Dzyu-do bo‘yicha 3-o‘rin g‘olibi</Paragraph>
                            </Card>
                        </Carousel>

                    </TabPane>
                    <TabPane tab="Ilmiy izlanishlar" key="2">
                        <p>“Yangi O‘zbekiston sharoitida migratsiya masalasi”</p>
                    </TabPane>
                    <TabPane tab="O'quv dasturi" key="3">
                        Content of Tab Pane 3
                    </TabPane>
                </Tabs>
                <Col>
                    <Button className="custom-bordered-btn">
                        Barchasini ko'rish
                    </Button>
                </Col>
            </Row>
        </React.Fragment>
    );
}

export default Students;