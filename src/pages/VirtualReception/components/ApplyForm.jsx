import React from 'react';
import { Row, Col, Card, Form, Button, Select, Input } from "antd";

import { steps } from "../VirtualReception";

const { Option } = Select;


export default class ApplyForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            captcha: null,
            firstNumber: null,
            secondNumber: null,
        }
    }

    getRandomInt = (max) => {
        return Math.floor(Math.random() * Math.floor(max));
    }

    renderCaptchaNumbers = () => {
        this.setState({
            firstNumber: this.getRandomInt(10),
            secondNumber: this.getRandomInt(15),
        })
    }

    checkCaptcha = (e) => {
        const { firstNumber, secondNumber } = this.state;

        const value = Number(e.target.value);
        const result = firstNumber + secondNumber;

        if (value === result) {
            this.setState({ captcha: true });
        }
    }

    componentDidMount() {
        this.renderCaptchaNumbers();
    }

    render() {
        const {
            current,
        } = this.props;

        const {
            firstNumber,
            secondNumber,
            captcha,
        } = this.state;

        return (
            <Form name="basic" onFinish={this.props.applyApplication} layout="vertical">
                <Card className="form-card">
                    <Row gutter={[48, 0]}>
                        <Col xs={24} lg={12}>
                            <Form.Item name="education_type" label="Ta`lim shakli" initialValue={this.props.education_type} rules={[{ required: true, message: "Ta`lim shaklini tanlang" }]}>
                                <Select placeholder="Ta`lim shakli" onChange={(val) => this.props.selectChangeHandler(val, "education_type")}>
                                    <Option value="kunduzgi">Kunduzgi</Option>
                                    <Option value="kechgi">Kechgi</Option>
                                </Select>
                            </Form.Item>

                            <Form.Item name="education_lang" label="Ta`lim tili" initialValue={this.props.education_lang} rules={[{ required: true, message: "Ta`lim tilini tanlang" }]}>
                                <Select placeholder="Ta`lim tili" onChange={(val) => this.props.selectChangeHandler(val, "education_lang")}>
                                    <Option value="uz">Uz</Option>
                                    <Option value="ru">Ru</Option>
                                </Select>
                            </Form.Item>

                            <Form.Item name="education_foreign_lang" label="Chet tili" initialValue={this.props.education_foreign_lang} rules={[{ required: true, message: "Chet tilini tanlang" }]}>
                                <Select placeholder="Chet tili" onChange={(val) => this.props.selectChangeHandler(val, "education_foreign_lang")}>
                                    <Option value="en">Eng</Option>
                                </Select>
                            </Form.Item>
                        </Col>
                        <Col xs={24} lg={12}>
                            <Form.Item name="education_faculty" label="Yo`nalish" initialValue={this.props.education_faculty} rules={[{ required: true, message: "Yo`nalishni tanlang" }]}>
                                <Select placeholder="Yo`nalish" onChange={(val) => this.props.selectChangeHandler(val, "education_faculty")}>
                                    <Option value="it">IT</Option>
                                </Select>
                            </Form.Item>

                            <div className="captcha">
                                <div className="numbers-box">
                                    {firstNumber} + {secondNumber}
                                </div>

                                <Form.Item name="captcha" label="Yuqoridagi ifodani hisoblang" rules={[{ required: true, message: "Ifodani hisoblang" }, { max: 50, message: "Maksimum 50 belgi" }]} >
                                    <Input name="captcha" placeholder="Familya" onChange={this.checkCaptcha} />
                                </Form.Item>
                            </div>

                        </Col>
                    </Row>
                </Card>

                <Row className="steps-action pb-5" justify="end">
                    <Col>
                        {
                            current > 0 && (
                                <Button className="mx-2" onClick={() => this.props.prev()}>
                                    Orqaga
                                </Button>
                            )
                        }

                        {
                            current < steps.length - 1 && (
                                <Button type="primary" onClick={this.props.next}>
                                    Keyingi
                                </Button>
                            )
                        }
                        {
                            current === steps.length - 1 && (
                                <Button type="primary" htmlType="submit" disabled={!captcha}>
                                    Yakunlash
                                </Button>
                            )
                        }
                    </Col>
                </Row>
            </Form>
        )
    }
}