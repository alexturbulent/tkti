import React from 'react';
import { Card, Row, Col, Button, Typography } from "antd";
const { Paragraph } = Typography;

const News = () => {
    return (
        <React.Fragment>
            <div className="header-section visible-lg">
                <div className="container py-3">
                    <Row>
                        <Col lg={12} >
                            <h3 className="section-header">Yangiliklar</h3>
                        </Col>
                        <Col lg={12} >
                            <h3 className="section-header">E'lonlar</h3>
                        </Col>
                    </Row>
                </div>
            </div>

            {/* News section & Announcem    ent section on desktop */}
            <div className="visible-lg">
                <Row className="container py-3">

                    <Col md={24} lg={12}>
                        <Row>
                            <Col md={24} lg={12}>
                                <Card size="small" className="news-card">
                                    <div className="text pl-2">
                                        <Paragraph ellipsis={{ rows: 5 }}>Toshkent kimyo-texnologiya institutining iqtidorli talabalari diqqatiga</Paragraph>
                                    </div>
                                    <p className="time m-0">06.06.2020</p>
                                </Card>
                            </Col>
                        </Row>
                    </Col>

                    <Col md={24} lg={12} >
                        <Row>
                            <Col md={24} lg={12}>
                                <Card size="small" className="announcement-card">
                                    <div className="text pl-2">
                                        <Paragraph ellipsis={{ rows: 5 }}>Toshkent kimyo-texnologiya institutining iqtidorli talabalari diqqatiga</Paragraph>
                                    </div>
                                    <p className="time m-0">06.06.2020</p>
                                </Card>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </div>

            {/* News section on mobile */}
            <div className="header-section visible-xs visible-sm visible-md">
                <div className="container py-3">
                    <Row>
                        <Col>
                            <h3 className="section-header">Yangiliklar</h3>
                        </Col>
                    </Row>
                </div>
            </div>

            <Row className="container py-3 visible-xs visible-sm visible-md">
                <Col md={24} lg={12}>
                    <Card size="small" className="news-card">
                        <div className="text pl-2">
                            <Paragraph ellipsis={{ rows: 5 }}>Toshkent kimyo-texnologiya institutining iqtidorli talabalari diqqatiga</Paragraph>
                        </div>
                        <p className="time m-0">06.06.2020</p>
                    </Card>
                </Col>
            </Row>

            {/* Announcement section on mobile */}
            <div className="header-section visible-xs visible-sm visible-md">
                <div className="container py-3">
                    <Row>
                        <Col>
                            <h3 className="section-header">E'lonlar</h3>
                        </Col>
                    </Row>
                </div>
            </div>

            <Row className="container py-3 visible-xs visible-sm visible-md">
                <Col md={24} lg={12}>
                    <Card size="small" className="announcement-card">
                        <div className="text pl-2">
                            <Paragraph ellipsis={{ rows: 5 }}>Toshkent kimyo-texnologiya institutining iqtidorli talabalari diqqatiga</Paragraph>
                        </div>
                        <p className="time m-0">06.06.2020</p>
                    </Card>
                </Col>
            </Row>

            <div className="container">
                <Row justify="end">
                    <Col className="mt-2">
                        <Button className="custom-bordered-btn">
                            Barchasini ko'rish
                    </Button>
                    </Col>
                </Row>
            </div>

        </React.Fragment>
    );
}

export default News;