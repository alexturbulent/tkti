import React from 'react';
import { Row, Col, Tabs, Card, Button } from "antd";

const { TabPane } = Tabs;

const Events = () => {
    return (
        <React.Fragment>
            <Row>
                <Col>
                    <h3 className="section-header">
                        Tadbirlar
                    </h3>
                </Col>
            </Row>
            <Tabs defaultActiveKey="1">
                <TabPane tab="Universitet" key="1">
                    <Row gutter={[24, 24]}>
                        <Col sm={24} md={12} lg={6}>
                            <Card
                                className="event-card"
                                hoverable
                                cover={<img alt="example" src="https://images.unsplash.com/photo-1474650919751-b7e21a1b180f?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=delfi-de-la-rua-A_InfAQM_lU-unsplash.jpg&w=640" />}
                            >
                                <div className="text-box">
                                    <p className="title">Yangi O‘zbekiston sharoitida migratsiya masalasi</p>
                                </div>
                            </Card>
                        </Col>
                        <Col sm={24} md={12} lg={6}>
                            <Card
                                className="event-card"
                                hoverable
                                cover={<img alt="example" src="https://images.unsplash.com/photo-1474650919751-b7e21a1b180f?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=delfi-de-la-rua-A_InfAQM_lU-unsplash.jpg&w=640" />}
                            >
                                <div className="text-box">
                                    <p className="title">Yangi O‘zbekiston sharoitida migratsiya masalasi</p>
                                </div>
                            </Card>
                        </Col>
                        <Col sm={24} md={12} lg={6}>
                            <Card
                                className="event-card"
                                hoverable
                                cover={<img alt="example" src="https://images.unsplash.com/photo-1474650919751-b7e21a1b180f?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=delfi-de-la-rua-A_InfAQM_lU-unsplash.jpg&w=640" />}
                            >
                                <div className="text-box">
                                    <p className="title">Yangi O‘zbekiston sharoitida migratsiya masalasi</p>
                                </div>
                            </Card>
                        </Col>
                        <Col sm={24} md={12} lg={6}>
                            <Card
                                className="event-card"
                                hoverable
                                cover={<img alt="example" src="https://images.unsplash.com/photo-1474650919751-b7e21a1b180f?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=delfi-de-la-rua-A_InfAQM_lU-unsplash.jpg&w=640" />}
                            >
                                <div className="text-box">
                                    <p className="title">Yangi O‘zbekiston sharoitida migratsiya masalasi</p>
                                </div>
                            </Card>
                        </Col>
                    </Row>
                </TabPane>
                <TabPane tab="Ilmiy izlanishlar" key="2">
                    <p>“Yangi O‘zbekiston sharoitida migratsiya masalasi”</p>
                </TabPane>
                <TabPane tab="O'quv dasturi" key="3">
                    Content of Tab Pane 3
                </TabPane>
            </Tabs>
            <Row justify="end">
                <Col>
                    <Button className="custom-bordered-btn">
                        Barchasini ko'rish
                    </Button>
                </Col>
            </Row>
        </React.Fragment>
    );
}

export default Events;