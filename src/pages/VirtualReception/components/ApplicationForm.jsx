import React from 'react';
import { Row, Col, Card, Form, Input, Button, DatePicker, Select, Upload, Switch, Divider } from "antd";
import { UploadOutlined, CheckOutlined, CloseOutlined } from '@ant-design/icons';
import moment from 'moment';

import "../reception.less";

import { steps } from "../VirtualReception";

const { Option } = Select;

export default (props) => {
    const {
        showMilitary,
        showDisabled,
        showSertificates,
        current,
    } = props;

    return (
        <Form name="basic" onFinish={() => console.log("go")} layout="vertical">

            {/* Personal info */}
            <Card className="form-card">
                <div className="bg-text">1</div>
                <h3 className="form-heading">Shaxsiy ma'lumotlar</h3>
                <Row gutter={[48, 0]}>
                    <Col xs={24} lg={12}>
                        <Form.Item name="first_name" label="Ism" initialValue={props.first_name} rules={[{ required: true, message: "Ismni kiriting" }, { max: 50, message: "Maksimum 50 belgi" }]}>
                            <Input name="first_name" autoFocus placeholder="Ism" onChange={props.inputChangeHandler} />
                        </Form.Item>

                        <Form.Item name="last_name" label="Familya" initialValue={props.last_name} rules={[{ required: true, message: "Familyani kiriting" }, { max: 50, message: "Maksimum 50 belgi" }]} >
                            <Input name="last_name" value={props.last_name} placeholder="Familya" onChange={props.inputChangeHandler} />
                        </Form.Item>

                        <Form.Item name="patronymic_name" label="Sharif" initialValue={props.last_name} rules={[{ required: true, message: "Sharifni kiriting" }, { max: 50, message: "Maksimum 50 belgi" }]}>
                            <Input name="patronymic_name" value={props.patronymic_name} placeholder="Sharif" onChange={props.inputChangeHandler} />
                        </Form.Item>
                    </Col>
                    <Col xs={24} lg={12}>
                        <Form.Item name="date_of_birth" label="Tug'ilgan sana" initialValue={props.date_of_birth ? moment(props.date_of_birth, 'YYYY-MM-DD') : null} rules={[{ required: true, message: "Sanani tanlang" }]}>
                            <DatePicker placeholder="Sanani tanlang" allowClear={false} showToday={false} name="date_of_birth" onChange={(value, dateString) => props.dateChangeHandler(value, dateString, "date_of_birth")} />
                        </Form.Item>

                        <Form.Item name="gender" label="Jins" initialValue={props.gender} rules={[{ required: true, message: "Jinsni tanglang" }]} >
                            <Select onChange={(val) => props.selectChangeHandler(val, "gender")}>
                                <Option value="man">Erkak</Option>
                                <Option value="woman">Ayol</Option>
                            </Select>
                        </Form.Item>

                        <Form.Item
                            name="photo_file"
                            label="Fotosurat (3,5 x 4,5)"
                            valuePropName="fileList"
                            getValueFromEvent={props.normFile}
                            initialValue={props.photo_file}
                            rules={[{ required: true, message: "Faylni tanglang" }]}
                            extra="Rasm jpeg, jpg, png formatida bo'lishi va 5mb dan oshmasligi zarur"
                        >
                            <Upload name="photo_file" listType="picture">
                                <Button>
                                    <UploadOutlined /> Faylni yuklash
                                </Button>
                            </Upload>
                        </Form.Item>
                    </Col>
                </Row>
            </Card>

            {/* Passport info */}
            <Card className="form-card">
                <div className="bg-text">2</div>
                <h3 className="form-heading">Passport ma'lumotlari</h3>
                <Row gutter={[48, 0]}>
                    <Col xs={24} lg={12}>
                        <Form.Item name="citizenship" label="Fuqaroligi" initialValue={props.citizenship} rules={[{ required: true, message: "Fuqarolikni tanlang" }]}>
                            <Select placeholder="Fuqaroligi" onChange={(val) => props.selectChangeHandler(val, "citizenship")}>
                                <Option value="uz">O'zbekiston</Option>
                                <Option value="ru">Russia</Option>
                            </Select>
                        </Form.Item>

                        <Form.Item name="city" label="Viloyat" initialValue={props.city} rules={[{ required: true, message: "Viloyatni tanlang" }]}>
                            <Select placeholder="Viloyat" onChange={(val) => props.selectChangeHandler(val, "city")}>
                                <Option value="toshkent">Toshkent</Option>
                                <Option value="buxoro">Buxoro</Option>
                            </Select>
                        </Form.Item>

                        <Form.Item name="region" label="Tuman" initialValue={props.region} rules={[{ required: true, message: "Tumanni tanlang" }]}>
                            <Select placeholder="Tuman" onChange={(val) => props.selectChangeHandler(val, "region")}>
                                <Option value="toshkent">Toshkent</Option>
                                <Option value="buxoro">Buxoro</Option>
                            </Select>
                        </Form.Item>

                        <Form.Item name="address" label="Ko'cha nomi, uy, xonadon raqami" initialValue={props.address} rules={[{ required: true, message: "Manzilni kiriting" }, { max: 250, message: "Maksimum 250 belgi" }]}>
                            <Input name="address" placeholder="Manzil" onChange={props.inputChangeHandler} />
                        </Form.Item>
                    </Col>
                    <Col xs={24} lg={12}>
                        <Form.Item name="nationality" label="Millat" initialValue={props.nationality} rules={[{ required: true, message: "Millatni tanlang" }]}>
                            <Select placeholder="Millat" onChange={(val) => props.selectChangeHandler(val, "nationality")}>
                                <Option value="uzbek">O'zbek</Option>
                                <Option value="rus">Rus</Option>
                            </Select>
                        </Form.Item>

                        <Row gutter={[8, 0]}>
                            <Col span={4}>
                                <Form.Item name="passport_serial" label="Seriya" initialValue={props.passport_serial} rules={[{ required: true, message: "Passport ma'lumotni kiriting" }, { max: 2, message: "Maksimum 2 belgi" }]}>
                                    <Input name="passport_serial" placeholder="AA" onChange={props.inputChangeHandler} />
                                </Form.Item>
                            </Col>
                            <Col flex="auto">
                                <Form.Item name="passport_number" label="Passport raqami" initialValue={props.passport_number} rules={[{ required: true, message: "Passport raqamini kiriting" }, { max: 7, message: "Maksimum 7 belgi" }]}>
                                    <Input name="passport_number" placeholder="1234567" onChange={props.inputChangeHandler} />
                                </Form.Item>
                            </Col>
                        </Row>

                        <Form.Item name="passport_by" label="Kim tomonidan berilgan" initialValue={props.passport_by} rules={[{ required: true, message: "Kim tomonidan berilganini kiriting" }, { max: 50, message: "Maksimum 50 belgi" }]}>
                            <Input name="passport_by" placeholder="Toshkent IIB" onChange={props.inputChangeHandler} />
                        </Form.Item>

                        <Form.Item
                            name="passport_file"
                            label="Passport (pdf)"
                            valuePropName="fileList"
                            getValueFromEvent={props.normFile}
                            initialValue={props.passport_file}
                            rules={[{ required: true, message: "Faylni tanglang" }]}
                        >
                            <Upload name="passport_file" listType="picture">
                                <Button>
                                    <UploadOutlined /> Faylni yuklash
                                                </Button>
                            </Upload>
                        </Form.Item>
                    </Col>
                </Row>
            </Card>

            {/* Education */}
            <Card className="form-card">
                <div className="bg-text">3</div>
                <h3 className="form-heading">Ma'lumotlari</h3>
                <Row gutter={[48, 0]}>
                    <Col xs={24} lg={12}>
                        <Form.Item name="edu_level" label="Ma'lumoti" initialValue={props.edu_level} rules={[{ required: true, message: "Ma'lumotni tanlang" }]}>
                            <Select placeholder="Ma'lumoti" onChange={(val) => props.selectChangeHandler(val, "edu_level")}>
                                <Option value="bakalavr">Bakalavr</Option>
                                <Option value="magistr">Magistr</Option>
                            </Select>
                        </Form.Item>

                        <Form.Item name="edu_org_name" label="Muassasa nomi" initialValue={props.edu_org_name} rules={[{ required: true, message: "Muassasa nomini kiriting" }, { max: 50, message: "Maksimum 50 belgi" }]}>
                            <Input name="edu_org_name" placeholder="Muassasa nomi" onChange={props.inputChangeHandler} />
                        </Form.Item>

                        <Form.Item name="edu_org_city" label="Muassasa joylashgan viloyat" initialValue={props.edu_org_city} rules={[{ required: true, message: "Viloyatni tanlang" }]}>
                            <Select placeholder="Muassasa joylashgan viloyat" onChange={(val) => props.selectChangeHandler(val, "edu_org_city")}>
                                <Option value="toshkent">Toshkent</Option>
                                <Option value="buxoro">Buxoro</Option>
                            </Select>
                        </Form.Item>

                        <Form.Item name="edu_org_region" label="Muassasa joylashgan tuman" initialValue={props.edu_org_region} rules={[{ required: true, message: "Tumanni tanlang" }]}>
                            <Select placeholder="Muassasa joylashgan tuman" onChange={(val) => props.selectChangeHandler(val, "edu_org_region")}>
                                <Option value="toshkent">Toshkent</Option>
                                <Option value="buxoro">Buxoro</Option>
                            </Select>
                        </Form.Item>


                    </Col>
                    <Col xs={24} lg={12}>
                        <Form.Item name="edu_org_type" label="Muassasa turi" initialValue={props.edu_org_type} rules={[{ required: true, message: "Muassasa turini tanlang" }]}>
                            <Select placeholder="Muassasa turi" onChange={(val) => props.selectChangeHandler(val, "edu_org_type")}>
                                <Option value="maktab">Maktab</Option>
                            </Select>
                        </Form.Item>

                        <Form.Item name="edu_graduation_year" label="Tugatgan yili" initialValue={props.edu_graduation_year} rules={[{ required: true, message: "Tugatgan yilini kiriting" }]}>
                            <Input name="edu_graduation_year" placeholder="2020" onChange={props.inputChangeHandler} />
                        </Form.Item>

                        <Row gutter={[8, 0]}>
                            <Col span={6}>
                                <Form.Item name="degree_serial" label="Diplom" initialValue={props.degree_serial} rules={[{ required: true, message: "Diplom ma'lumotni kiriting" }, { max: 2, message: "Maksimum 2 belgi" }]}>
                                    <Input name="degree_serial" placeholder="BC" onChange={props.inputChangeHandler} />
                                </Form.Item>
                            </Col>
                            <Col flex="auto">
                                <Form.Item name="degree_number" label="Diplom raqami" initialValue={props.degree_number} rules={[{ required: true, message: "Diplom raqamini kiriting" }, { max: 15, message: "Maksimum 15 belgi" }]}>
                                    <Input name="Diplom" placeholder="1234567" onChange={props.inputChangeHandler} />
                                </Form.Item>
                            </Col>
                        </Row>

                        <Form.Item
                            name="degree_file"
                            label="Attestat yoki diplom (pdf)"
                            valuePropName="fileList"
                            getValueFromEvent={props.normFile}
                            initialValue={props.degree_file}
                            rules={[{ required: true, message: "Faylni tanglang" }]}
                        >
                            <Upload name="degree_file" listType="picture">
                                <Button>
                                    <UploadOutlined /> Faylni yuklash
                                                </Button>
                            </Upload>
                        </Form.Item>
                    </Col>
                </Row>
            </Card>

            {/* Additional info */}
            <Card className="form-card">
                <div className="bg-text">4</div>
                <h3 className="form-heading">Qo'shimcha ma'lumotlari</h3>

                {/* Military section */}
                <Row align="middle">
                    <Col xs={22}>
                        <h4 className="section-heading">Muddatli harbiy xizmat tavsiyanomasiga ega abituriyent</h4>
                    </Col>
                    <Col xs={2} className="switch-box">
                        <Switch
                            name="showMilitary"
                            checked={showMilitary}
                            checkedChildren={<CheckOutlined />}
                            unCheckedChildren={<CloseOutlined />}
                            onChange={(value) => props.switchChangeHandler(value, "showMilitary")}
                        />
                    </Col>
                </Row>

                {
                    !showMilitary && <Divider className="m-0" />
                }

                {
                    showMilitary && (
                        <Row gutter={[48, 0]}>
                            <Col xs={24} lg={12}>
                                <Form.Item name="military_serial" label="Hujjat seriyasi" initialValue={props.military_serial} rules={[{ required: true, message: "Hujjat seriyasini kiriting" }]}>
                                    <Input name="military_serial" placeholder="Hujjat seriyasi" onChange={props.inputChangeHandler} />
                                </Form.Item>

                                <Form.Item name="military_number" label="Hujjat raqami" initialValue={props.military_number} rules={[{ required: true, message: "Hujjat raqamini kiriting" }]}>
                                    <Input name="military_number" placeholder="Hujjat raqami" onChange={props.inputChangeHandler} />
                                </Form.Item>

                                <Form.Item name="military_unit" label="Harbiy qism" initialValue={props.military_unit} rules={[{ required: true, message: "Harbiy qismni kiriting" }]}>
                                    <Input name="military_unit" placeholder="Harbiy qism" onChange={props.inputChangeHandler} />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12}>
                                <Form.Item name="military_doc_date" label="Hujjat berilgan sana" initialValue={props.military_doc_date ? moment(props.military_doc_date, 'YYYY-MM-DD') : null} rules={[{ required: true, message: "Sanani tanlang" }]}>
                                    <DatePicker
                                        placeholder="Sanani tanlang"
                                        allowClear={false}
                                        showToday={false}
                                        name="military_doc_date"
                                        onChange={(value, dateString) => props.dateChangeHandler(value, dateString, "military_doc_date")}
                                    />
                                </Form.Item>

                                <Form.Item
                                    name="military_doc_file"
                                    label="Hujjat rasmi"
                                    valuePropName="fileList"
                                    getValueFromEvent={props.normFile}
                                    initialValue={props.military_doc_file}
                                    rules={[{ required: true, message: "Faylni tanglang" }]}
                                    extra="Rasm jpeg , jpg, png formatlarida bo`lishi va 5 mb dan oshmasligi zarur"
                                >
                                    <Upload name="degree_file" listType="picture">
                                        <Button>
                                            <UploadOutlined /> Faylni yuklash
                                        </Button>
                                    </Upload>
                                </Form.Item>
                            </Col>
                        </Row>
                    )
                }

                {/* Disabled section */}
                <Row align="middle">
                    <Col xs={22}>
                        <h4 className="section-heading">Nogironligi bor shaxslar uchun imtiyoz</h4>
                    </Col>
                    <Col xs={2} className="switch-box">
                        <Switch
                            name="showDisabled"
                            checked={showDisabled}
                            checkedChildren={<CheckOutlined />}
                            unCheckedChildren={<CloseOutlined />}
                            onChange={(value) => props.switchChangeHandler(value, "showDisabled")}
                        />
                    </Col>
                </Row>

                {
                    !showDisabled && <Divider className="m-0" />
                }

                {
                    showDisabled && (
                        <Row gutter={[48, 0]}>
                            <Col xs={24} lg={12}>
                                <Form.Item name="disabled_serial" label="Hujjat seriyasi" initialValue={props.disabled_serial} rules={[{ required: true, message: "Hujjat seriyasini kiriting" }]}>
                                    <Input name="disabled_serial" placeholder="Hujjat seriyasi" onChange={props.inputChangeHandler} />
                                </Form.Item>

                                <Form.Item name="disabled_number" label="Hujjat raqami" initialValue={props.disabled_number} rules={[{ required: true, message: "Hujjat raqamini kiriting" }]}>
                                    <Input name="disabled_number" placeholder="Hujjat raqami" onChange={props.inputChangeHandler} />
                                </Form.Item>

                                <Form.Item name="disabled_group" label="Nogironlik guruhi" initialValue={props.disabled_group} rules={[{ required: true, message: "Nogironlik guruhini kiriting" }]}>
                                    <Input name="disabled_group" placeholder="Nogironlik guruhi" onChange={props.inputChangeHandler} />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12}>
                                <Form.Item name="disabled_doc_date" label="Hujjat berilgan sana" initialValue={props.disabled_doc_date ? moment(props.disabled_doc_date, 'YYYY-MM-DD') : null} rules={[{ required: true, message: "Sanani tanlang" }]}>
                                    <DatePicker
                                        placeholder="Sanani tanlang"
                                        allowClear={false}
                                        showToday={false}
                                        name="disabled_doc_date"
                                        onChange={(value, dateString) => props.dateChangeHandler(value, dateString, "disabled_doc_date")}
                                    />
                                </Form.Item>

                                <Form.Item
                                    name="disabled_doc_file"
                                    label="Hujjat rasmi"
                                    valuePropName="fileList"
                                    getValueFromEvent={props.normFile}
                                    initialValue={props.disabled_doc_file}
                                    rules={[{ required: true, message: "Faylni tanglang" }]}
                                    extra="Rasm jpeg , jpg, png formatlarida bo`lishi va 5 mb dan oshmasligi zarur"
                                >
                                    <Upload name="degree_file" listType="picture">
                                        <Button>
                                            <UploadOutlined /> Faylni yuklash
                                        </Button>
                                    </Upload>
                                </Form.Item>
                            </Col>
                        </Row>
                    )
                }

                {/* Sertificates section */}
                <Row align="middle">
                    <Col xs={22}>
                        <h4 className="section-heading">Chet tili sertifikati</h4>
                    </Col>
                    <Col xs={2} className="switch-box">
                        <Switch
                            name="showSertificates"
                            checked={showSertificates}
                            checkedChildren={<CheckOutlined />}
                            unCheckedChildren={<CloseOutlined />}
                            onChange={(value) => props.switchChangeHandler(value, "showSertificates")}
                        />
                    </Col>
                </Row>


                {
                    showSertificates && (
                        <Row gutter={[48, 0]}>
                            <Col xs={24} lg={12}>
                                <Form.Item name="sertificate_lang" label="Chet tili" initialValue={props.sertificate_lang} rules={[{ required: true, message: "Tilni tanlang" }]}>
                                    <Select placeholder="Chet tili" onChange={(val) => props.selectChangeHandler(val, "sertificate_lang")}>
                                        <Option value="en">English</Option>
                                        <Option value="ru">Russia</Option>
                                    </Select>
                                </Form.Item>

                                <Form.Item name="sertificate_type" label="Sertifikat turi" initialValue={props.sertificate_type} rules={[{ required: true, message: "Turni tanlang" }]}>
                                    <Select placeholder="Sertifikat turi" onChange={(val) => props.selectChangeHandler(val, "sertificate_type")}>
                                        <Option value="en">IELTS</Option>
                                        <Option value="ru">TOEFL</Option>
                                    </Select>
                                </Form.Item>

                                <Form.Item name="sertificate_level" label="Daraja" initialValue={props.sertificate_level} rules={[{ required: true, message: "Darajani tanlang" }]}>
                                    <Select placeholder="Daraja" onChange={(val) => props.selectChangeHandler(val, "sertificate_level")}>
                                        <Option value="9">9</Option>
                                        <Option value="8">8</Option>
                                    </Select>
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12}>
                                <Form.Item name="sertificate_serial" label="Hujjat seriyasi" initialValue={props.sertificate_serial} rules={[{ required: true, message: "Hujjat seriyasini kiriting" }]}>
                                    <Input name="sertificate_serial" placeholder="Hujjat seriyasi" onChange={props.inputChangeHandler} />
                                </Form.Item>

                                <Form.Item name="sertificate_number" label="Hujjat raqami" initialValue={props.sertificate_number} rules={[{ required: true, message: "Hujjat raqamini kiriting" }]}>
                                    <Input name="sertificate_number" placeholder="Hujjat raqami" onChange={props.inputChangeHandler} />
                                </Form.Item>

                                <Form.Item name="sertificate_doc_date" label="Hujjat berilgan sana" initialValue={props.sertificate_doc_date ? moment(props.sertificate_doc_date, 'YYYY-MM-DD') : null} rules={[{ required: true, message: "Sanani tanlang" }]}>
                                    <DatePicker
                                        placeholder="Sanani tanlang"
                                        allowClear={false}
                                        showToday={false}
                                        name="sertificate_doc_date"
                                        onChange={(value, dateString) => props.dateChangeHandler(value, dateString, "sertificate_doc_date")}
                                    />
                                </Form.Item>

                                <Form.Item
                                    name="sertificate_doc_file"
                                    label="Hujjat rasmi"
                                    valuePropName="fileList"
                                    getValueFromEvent={props.normFile}
                                    rules={[{ required: true, message: "Faylni tanglang" }]}
                                    extra="Rasm jpeg , jpg, png formatlarida bo`lishi va 5 mb dan oshmasligi zarur"
                                >
                                    <Upload name="degree_file" listType="picture">
                                        <Button>
                                            <UploadOutlined /> Faylni yuklash
                                                </Button>
                                    </Upload>
                                </Form.Item>
                            </Col>
                        </Row>
                    )
                }

            </Card>

            {/* Contact info */}
            <Card className="form-card">
                <h3 className="form-heading">Bog'lanish uchun ma'lumotlar</h3>
                <Row gutter={[48, 0]}>
                    <Col xs={24} lg={12}>
                        <Form.Item name="phone_number" label="Uyali aloqa raqam" initialValue={props.phone_number} rules={[{ required: true, message: "Uyali aloqa raqamni kiriting" }]}>
                            <Input name="phone_number" placeholder="+998912345678" onChange={props.inputChangeHandler} />
                        </Form.Item>

                        <Form.Item name="email" label="Email" initialValue={props.email} rules={[{ type: "email" }]}>
                            <Input name="email" placeholder="user@example.com" onChange={props.inputChangeHandler} />
                        </Form.Item>
                    </Col>
                    <Col xs={24} lg={12}>
                        <Form.Item name="additional_phone_number" label="Qo'shimcha telefon raqam" initialValue={props.additional_phone_number} rules={[{ required: true, message: "Qo'shimcha telefon raqamni kiriting" }]}>
                            <Input name="additional_phone_number" placeholder="+998912345678" onChange={props.inputChangeHandler} />
                        </Form.Item>

                    </Col>
                </Row>
            </Card>

            <Row className="steps-action pb-5" justify="end">
                <Col>
                    {
                        current > 0 && (
                            <Button className="mx-2" onClick={() => props.prev()}>
                                Orqaga
                            </Button>
                        )
                    }

                    {
                        current < steps.length - 1 && (
                            <Button type="primary" htmlType="submit">
                                Keyingi
                            </Button>
                        )
                    }
                    {
                        current === steps.length - 1 && (
                            <Button type="primary">
                                Yakunlash
                            </Button>
                        )
                    }
                </Col>
            </Row>
        </Form>
    )
}