import React from 'react';
import { Row, Col } from 'antd';
import FacebookOutlined from '@ant-design/icons/FacebookOutlined';
import TwitterOutlined from '@ant-design/icons/TwitterOutlined';
import YoutubeOutlined from '@ant-design/icons/YoutubeOutlined';

const FooterSocialNetwork = () => {
    return (
        <Row justify="space-between">
            <Col>
                <h3 className="section-header">Biz ijtimoiy tarmoqlarda</h3>
            </Col>
            <Col>
                <Row gutter={[16, 8]}>
                    <Col>
                        <FacebookOutlined
                            style={{
                                fontSize: "40px",
                                color: "#3b5998",
                            }}
                        />
                    </Col>
                    <Col>
                        <TwitterOutlined
                            style={{
                                fontSize: "40px",
                                color: "#00acee",
                            }}
                        />
                    </Col>
                    <Col>
                        <YoutubeOutlined
                            style={{
                                fontSize: "40px",
                                color: "#c4302b",
                            }}
                        />
                    </Col>
                </Row>
            </Col>
        </Row>
    );
}

export default FooterSocialNetwork;