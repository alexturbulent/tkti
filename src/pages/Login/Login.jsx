import React, { useState } from 'react';
import { Row, Col, Form, Input, Button } from "antd";
import logo from "../../assets/img/logo.png";

import './login.less';

function Login() {
    const [login, setLogin] = useState(null);
    const [password, setPassword] = useState(null);

    function formChangeHandler(e) {
        const value = e.target.value;
        const name = e.target.name;
        if (name === 'login') {
            setLogin(value);
        } else {
            setPassword(value);
        }
    }

    function submit() {
        console.log(login, password);

    }

    return (
        <div className="wrap">
            <div className="top-nav container py-4 ">
                <Row gutter={8} justify="start" align="middle">
                    <Col>
                        <img alt="Img error" src={logo} />
                    </Col>
                    <Col>
                        <p className="logo-text">Toshkent <br /> Kimyo-Texnologiya <br />Instituti</p>
                    </Col>
                </Row>
            </div>

            <div className="container mt-5">
                <Row>
                    <Col md={24} lg={12}>
                        <h3 className="login-heading mb-5">Tizimga kirish uchun telefon raqamingizni kiriting</h3>

                        <Form
                            className="login-form"
                            initialValues={{ remember: true }}
                            onFinish={submit}
                            layout="vertical"
                        >
                            <Form.Item
                                name="login"
                                rules={[{ required: true, message: 'Telefoningizni kiriting' }]}
                            >
                                <Input name="login" placeholder="Sizning telefoningiz" className="login-input" />
                            </Form.Item>

                            <Form.Item
                                name="password"
                                className="mb-5"
                                rules={[{ required: true, message: 'Parolni kiriting' }]}
                            >
                                <Input.Password name="password" placeholder="Parol" className="login-input" onChange={formChangeHandler} />
                            </Form.Item>

                            <Form.Item>
                                <Button className="submit-btn" size="large" type="default" htmlType="submit">
                                    Kirish
                                </Button>
                            </Form.Item>
                        </Form>
                    </Col>
                </Row>
            </div>
        </div>
    )
}

export default Login;