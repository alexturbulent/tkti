import React from 'react';
import { Row, Col, Tabs, Typography, Button } from 'antd';

import playCicle from "../../../assets/img/play-circle.png";

const { TabPane } = Tabs;
const { Paragraph } = Typography;

const Videos = () => {
    return (
        <React.Fragment>
            <div className="header-section">
                <div className="container py-3">
                    <h3 className="section-header">Videos</h3>
                </div>
            </div>
            <Row className="container" justify="end">
                <Tabs defaultActiveKey="1">
                    <TabPane tab="Universitet" key="1">
                        <Row gutter={[24, 24]}>
                            <Col sm={24} md={12} lg={6}>
                                <div className="video-box">
                                    <div className="poster mb-3">
                                        <img alt="example" src="https://images.unsplash.com/photo-1568792923760-d70635a89fdc?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=darya-tryfanava-d55fhArDES0-unsplash.jpg&w=640" />
                                        <div className="play-box">
                                            <img alt="Img error" src={playCicle} />
                                        </div>
                                    </div>
                                    <Paragraph className="title" ellipsis={{ rows: 5 }}>Yangi O‘zbekiston sharoitida migratsiya masalasi</Paragraph>
                                    <p className="time">06.06.2020</p>
                                </div>
                            </Col>
                            <Col sm={24} md={12} lg={6}>
                                <div className="video-box">
                                    <div className="poster mb-3">
                                        <img alt="example" src="https://images.unsplash.com/photo-1568792923760-d70635a89fdc?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=darya-tryfanava-d55fhArDES0-unsplash.jpg&w=640" />
                                        <div className="play-box">
                                            <img alt="Img error" src={playCicle} />
                                        </div>
                                    </div>
                                    <Paragraph className="title" ellipsis={{ rows: 5 }}>Yangi O‘zbekiston sharoitida migratsiya masalasi</Paragraph>
                                    <p className="time">06.06.2020</p>
                                </div>
                            </Col>
                            <Col sm={24} md={12} lg={6}>
                                <div className="video-box">
                                    <div className="poster mb-3">
                                        <img alt="example" src="https://images.unsplash.com/photo-1568792923760-d70635a89fdc?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=darya-tryfanava-d55fhArDES0-unsplash.jpg&w=640" />
                                        <div className="play-box">
                                            <img alt="Img error" src={playCicle} />
                                        </div>
                                    </div>
                                    <Paragraph className="title" ellipsis={{ rows: 5 }}>Yangi O‘zbekiston sharoitida migratsiya masalasi</Paragraph>
                                    <p className="time">06.06.2020</p>
                                </div>
                            </Col>
                            <Col sm={24} md={12} lg={6}>
                                <div className="video-box">
                                    <div className="poster mb-3">
                                        <img alt="example" src="https://images.unsplash.com/photo-1568792923760-d70635a89fdc?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=darya-tryfanava-d55fhArDES0-unsplash.jpg&w=640" />
                                        <div className="play-box">
                                            <img alt="Img error" src={playCicle} />
                                        </div>
                                    </div>
                                    <Paragraph className="title" ellipsis={{ rows: 5 }}>Yangi O‘zbekiston sharoitida migratsiya masalasi</Paragraph>
                                    <p className="time">06.06.2020</p>
                                </div>
                            </Col>
                        </Row>
                    </TabPane>
                    <TabPane tab="Ilmiy izlanishlar" key="2">
                        <p>“Yangi O‘zbekiston sharoitida migratsiya masalasi”</p>
                    </TabPane>
                    <TabPane tab="O'quv dasturi" key="3">
                        Content of Tab Pane 3
                    </TabPane>
                </Tabs>
                <Col>
                    <Button className="custom-bordered-btn">
                        Barchasini ko'rish
                    </Button>
                </Col>
            </Row>
        </React.Fragment>
    );
}

export default Videos;