import React, { useState } from 'react';
import { Drawer, Button, Menu } from "antd";
import { Link } from 'react-router-dom';

import TopNavbar from "./components/TopNavbar";
import MainNavbar from "./components/MainNavbar";
import Carousel from "./components/Carousel";
import FindFaculty from "./components/FindFaculty";
import News from "./components/News";
import Events from "./components/Events";
import Services from "./components/Services";
import Videos from "./components/Videos";
import Students from "./components/Students";
import FooterSocialNetwork from './components/FooterSocialNetwork';
import FooterInfo from './components/FooterInfo';

import "./home.less";

const { SubMenu } = Menu;

function Home() {
    const [visible, setVisible] = useState(false);

    const showDrawer = () => {
        setVisible(true);
    }

    const onClose = () => {
        setVisible(false);
    }

    return (
        <React.Fragment>
            <div className="top-special-navbar">
                <TopNavbar />
            </div>

            <div className="main-navbar container my-2">
                <MainNavbar showDrawer={showDrawer} />
            </div>

            <div className="container">
                <Carousel />
            </div>

            <div className="find-faculty-section my-5">
                <div className="container py-5">
                    <FindFaculty />
                </div>
            </div>

            <div className="news">
                <News />
            </div>

            <div className="events my-5">
                <div className="container py-4">
                    <Events />
                </div>
            </div>

            <div className="services">
                <Services />
            </div>

            <div className="videos mb-5">
                <Videos />
            </div>

            <div className="students">
                <Students />
            </div>

            <div className="footer-social-network my-5 py-4">
                <div className="container">
                    <FooterSocialNetwork />
                </div>
            </div>

            <div className="footer-info container mb-5">
                <FooterInfo />
            </div>

            <Drawer
                title="Menu"
                placement="right"
                closable={false}
                onClose={onClose}
                visible={visible}
            >
                <div className="mb-2">
                    <Button type="primary" className="sidebar-btn">
                        Covid-19
                    </Button>
                    <Button className="sidebar-btn">
                        Pochta
                    </Button>
                    <Button className="sidebar-btn">
                        Virtual qabulxona
                    </Button>
                </div>
                <Menu mode="inline">
                    <SubMenu title="Umumiy">
                        <Menu.Item>
                            <Link to='/'>
                                Menu 1
                            </Link>
                        </Menu.Item>
                    </SubMenu>
                    <Menu.Item>
                        <Link to='/'>
                            Menu 2
                        </Link>
                    </Menu.Item>
                </Menu>
            </Drawer>
        </React.Fragment>
    )
}

export default Home;