import React from 'react';
import { Row, Col, Card, Button } from "antd";

import user from "../../../assets/img/student.png";
import "../reception.less";

import { steps } from "../VirtualReception";

export default (props) => {
    const {
        showMilitary,
        showDisabled,
        showSertificates,
        current,
    } = props;



    const state = {
        first_name: "Sanjar",
        last_name: "Tojiboyev",
        patronymic_name: "Axrorovich",
        date_of_birth: "15.06.2000",
        gender: "Erkak",
        photo_file: null,

        citizenship: "O'zbekiston",
        city: "Toshkent",
        region: "Yunusobod",
        address: "Bodomzor 14/ 6",
        nationality: "O'zbek",
        passport_serial: "AA",
        passport_number: "6578899",
        passport_by: "Yunusobod tumani IIB",
        passport_file: null,

        edu_level: "O'rta maxsus",
        edu_org_name: "SamISI",
        edu_org_city: "Samarqand viloyati",
        edu_org_region: "Samarqand shahri",
        edu_org_type: "Akademik litsey",
        edu_graduation_year: "2000",
        degree_serial: "A",
        degree_number: "657365",
        degree_file: null,

        military_serial: "AA",
        military_number: "6578899",
        military_unit: "15 post",
        military_doc_date: "12.12.2020",
        military_doc_file: null,

        disabled_serial: "AA",
        disabled_number: "202021",
        disabled_group: "2",
        disabled_doc_date: "20.12.2002",
        disabled_doc_file: null,

        sertificate_lang: "Uz",
        sertificate_type: "IELTS",
        sertificate_level: "5",
        sertificate_serial: "GG",
        sertificate_number: "92922",
        sertificate_doc_date: "12.12.2005",
        sertificate_doc_file: null,

        phone_number: "+998 99 959 59 95",
        email: "example@gmail.com",
        additional_phone_number: "+998 99 959 59 95",
    }

    return (
        <React.Fragment>

            {/* Main info */}
            <Card className="review-card">
                <Row gutter={[48, 0]}>
                    <Col xs={24} md={6} className="user-img-box">
                        <img alt="Error" src={user} />
                    </Col>
                    <Col xs={24} md={18}>
                        <h3 className="form-heading">{state.last_name} {state.first_name} {state.patronymic_name}</h3>

                        <Row>
                            <Col xs={24} md={12}>
                                <p className="text-info">Tug`ulgan sanasi: <span className="value">{state.date_of_birth}</span></p>
                            </Col>
                            <Col xs={24} md={12}>
                                <p className="text-info">Jinsi: <span className="value">{state.gender}</span></p>
                            </Col>
                            <Col xs={24} md={12}>
                                <p className="text-info">Viloyat va tuman/shahri: <span className="value">{state.city}, {state.region}</span></p>
                            </Col>
                            <Col xs={24} md={12}>
                                <p className="text-info">Ko`cha nomi, uy va xonadon raqami: <span className="value">{state.address}</span></p>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Card>

            {/* Passport info */}
            <Card className="review-card">
                <h3 className="form-heading">Pasport ma`lumotlari</h3>

                <Row>
                    <Col xs={24} md={12}>
                        <p className="text-info">Fuqaroligi: <span className="value">{state.citizenship}</span></p>
                    </Col>
                    <Col xs={24} md={12}>
                        <p className="text-info">Millati: <span className="value">{state.nationality}</span></p>
                    </Col>
                    <Col xs={24} md={12}>
                        <p className="text-info">Passport yoki guvohnoma seriyasi va raqami: <span className="value">{state.passport_serial} {state.passport_number}</span></p>
                    </Col>
                    <Col xs={24} md={12}>
                        <p className="text-info">Kim tomonidan berilgan: <span className="value">{state.passport_by}</span></p>
                    </Col>
                </Row>
            </Card>

            {/* Education info */}
            <Card className="review-card">
                <h3 className="form-heading">Ma`lumotlari</h3>

                <Row>
                    <Col xs={24} md={12}>
                        <p className="text-info">Ma`lumoti: <span className="value">{state.edu_level}</span></p>
                    </Col>
                    <Col xs={24} md={12}>
                        <p className="text-info">Muassasa nomi: <span className="value">{state.edu_org_name}</span></p>
                    </Col>
                    <Col xs={24} md={12}>
                        <p className="text-info">Diplom yoki attestat seriyasi raqami: <span className="value">{state.degree_serial} {state.degree_number}</span></p>
                    </Col>
                    <Col xs={24} md={12}>
                        <p className="text-info">Tugatgan muassasasi joylashgan hududi: <span className="value">{state.edu_org_city}, {state.edu_org_region}</span></p>
                    </Col>
                    <Col xs={24} md={12}>
                        <p className="text-info">Muassasa turi: <span className="value">{state.edu_org_type}</span></p>
                    </Col>
                    <Col xs={24} md={12}>
                        <p className="text-info">Tugatgan yili: <span className="value">{state.edu_graduation_year}</span></p>
                    </Col>
                </Row>
            </Card>

            {/* Disabled info */}
            <Card className="review-card">
                <h3 className="form-heading">Qo`shimcha ma`lumotlar</h3>
                {
                    showMilitary && (
                        <React.Fragment>
                            <h4 className="additional-section-heading">Harbiy xizmat tavsiyanomasi</h4>
                            <Row>
                                <Col xs={24} md={12}>
                                    <p className="text-info">Harbiy qism: <span className="value">{state.military_unit}</span></p>
                                </Col>
                                <Col xs={24} md={12}>
                                    <p className="text-info">Hujjati: <span className="value">{state.military_serial} {state.military_number}</span></p>
                                </Col>
                                <Col xs={24} md={12}>
                                    <p className="text-info">Berilgan sana: <span className="value">{state.military_doc_date}</span></p>
                                </Col>
                            </Row>
                        </React.Fragment>
                    )
                }
                {
                    showDisabled && (
                        <React.Fragment>
                            <h4 className="additional-section-heading">Nogironlik haqida ma'lumot</h4>
                            <Row>
                                <Col xs={24} md={12}>
                                    <p className="text-info">Nogironlik guruhi: <span className="value">{state.disabled_group}</span></p>
                                </Col>
                                <Col xs={24} md={12}>
                                    <p className="text-info">Hujjati: <span className="value">{state.disabled_serial} {state.disabled_number}</span></p>
                                </Col>
                                <Col xs={24} md={12}>
                                    <p className="text-info">Berilgan sana: <span className="value">{state.disabled_doc_date}</span></p>
                                </Col>
                            </Row>
                        </React.Fragment>
                    )
                }
                {
                    showSertificates && (
                        <React.Fragment>
                            <h4 className="additional-section-heading">Chet tili sertifikatlari</h4>
                            <Row>
                                <Col xs={24} md={12}>
                                    <p className="text-info">Chet tili: <span className="value">{state.sertificate_lang}</span></p>
                                </Col>
                                <Col xs={24} md={12}>
                                    <p className="text-info">Sertifikat turi: <span className="value">{state.sertificate_type}</span></p>
                                </Col>
                                <Col xs={24} md={12}>
                                    <p className="text-info">Daraja: <span className="value">{state.sertificate_level}</span></p>
                                </Col>
                                <Col xs={24} md={12}>
                                    <p className="text-info">Hujjati: <span className="value">{state.disabled_serial} {state.disabled_number}</span></p>
                                </Col>
                                <Col xs={24} md={12}>
                                    <p className="text-info">Berilgan sana: <span className="value">{state.disabled_doc_date}</span></p>
                                </Col>
                            </Row>
                        </React.Fragment>
                    )
                }

                {
                    !showMilitary && !showDisabled && !showSertificates && (
                        <h4 className="additional-section-heading">Ma'lumot yo'q</h4>
                    )
                }
            </Card>

            {/* Contact info */}
            <Card className="review-card">
                <h3 className="form-heading">Bog`lanish uchun ma`lumotlar</h3>

                <Row>
                    <Col xs={24} md={12}>
                        <p className="text-info">Uyali telefon raqami: <span className="value">{state.phone_number}</span></p>
                    </Col>
                    <Col xs={24} md={12}>
                        <p className="text-info">Qo`shimcha telefon raqami: <span className="value">{state.additional_phone_number}</span></p>
                    </Col>
                    <Col xs={24} md={12}>
                        <p className="text-info">E-mail manzili: <span className="value">{state.email}</span></p>
                    </Col>
                </Row>
            </Card>

            <Row className="steps-action pb-5" justify="end">
                <Col>
                    {
                        current > 0 && (
                            <Button className="mx-2" onClick={() => props.prev()}>
                                Orqaga
                            </Button>
                        )
                    }

                    {
                        current < steps.length - 1 && (
                            <Button type="primary" htmlType="submit" onClick={props.next}>
                                Keyingi
                            </Button>
                        )
                    }
                    {
                        current === steps.length - 1 && (
                            <Button type="primary">
                                Yakunlash
                            </Button>
                        )
                    }
                </Col>
            </Row>
        </React.Fragment>
    )
}
