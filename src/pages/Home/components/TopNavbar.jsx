import React from 'react';
import { Button, Row, Col, Dropdown, Menu } from 'antd';
import { EyeOutlined, DownOutlined } from '@ant-design/icons';

const menu = (
    <Menu>
        <Menu.Item>
            <a rel="noopener noreferrer" href="http://www.alipay.com/">
                Uz
            </a>
        </Menu.Item>
        <Menu.Item>
            <a rel="noopener noreferrer" href="http://www.taobao.com/">
                Ru
            </a>
        </Menu.Item>
        <Menu.Item>
            <a rel="noopener noreferrer" href="http://www.tmall.com/">
                En
            </a>
        </Menu.Item>
    </Menu>
);

const TopNavbar = () => {
    return (
        <div className="container">
            <Row justify="end">
                <Col>
                    <Button size="small" type="text" key="3">A-</Button>
                </Col>
                <Col>
                    <Button size="small" type="text" key="2">A</Button>
                </Col>
                <Col>
                    <Button size="small" type="text" key="1">A+</Button>
                </Col>
                <Col>
                    <Button size="small" type="text" key="5" icon={<EyeOutlined />} />
                </Col>
                <Col>
                    {/* <Button size="small" type="text" key="4">O'z</Button> */}
                    <Dropdown overlay={menu}>
                        <Button type="text" size="small">
                            Uz <DownOutlined />
                        </Button>
                    </Dropdown>
                </Col>
            </Row>
        </div>
    );
}

export default TopNavbar;
