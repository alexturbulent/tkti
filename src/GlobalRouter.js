import React, { Suspense, lazy } from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect,
} from "react-router-dom";
import './App.less';

/* PAGES */
import Home from "./pages/Home/Home";
import VirtualReception from './pages/VirtualReception/VirtualReception';
import Login from './pages/Login/Login';
const Dashboard = lazy(() => import('./pages/Dashboard/Dashboard'));

const isAuthenticated = true;

function GlobalRouter() {
    return (
        <Router>
            <Switch>
                <Route path="/login" children={() => <Login />} />
                <Route path="/virtual-reception" children={() => <VirtualReception />} />
                <Route exact path="/" children={() => <Home />} />

                {
                    !isAuthenticated && (
                        <Route>
                            <Redirect to="/login" />
                        </Route>
                    )
                }

                <Route path="/dashboard" render={() => (
                    <Suspense fallback={"Loading..."}>
                        <Dashboard />
                    </Suspense>
                )} />
            </Switch>
        </Router>
    );
}

export default GlobalRouter;