import React from 'react';
import { Row, Col, Card } from "antd";

import chemestrywhite from "../../../assets/img/chemistry-white.png";
import learning from "../../../assets/img/e-learning.png";
import hierarchy from "../../../assets/img/hierarchy.png";
import graduate from "../../../assets/img/man-graduate.png";
import moddle from "../../../assets/img/moddle.png";
import reception from "../../../assets/img/reception.png";
import checklist from "../../../assets/img/task-checklist-check.png";

const Services = () => {
    return (
        <React.Fragment>
            <div className="header-section">
                <div className="container py-3">
                    <h3 className="section-header">Interaktiv xizmatlar</h3>
                </div>
            </div>
            <div className="container py-3">
                <Row gutter={[24, 24]}>
                    <Col xs={24} md={12} lg={8}>
                        <Card className="servive-card p-4" style={{ backgroundColor: "#B8DCEE" }}>
                            <img alt="Img error" className="bg" src={chemestrywhite} />
                            <img alt="Img error" className="mb-2" src={moddle} />
                            <p className="title">Moddle tizimi</p>
                        </Card>
                    </Col>
                    <Col xs={24} md={12} lg={8}>
                        <Card className="servive-card p-4" style={{ backgroundColor: "#A9E5BE" }}>
                            <img alt="Img error" className="bg" src={chemestrywhite} />
                            <img alt="Img error" className="mb-2" src={hierarchy} />
                            <p className="title">Elektron dekanat</p>
                        </Card>
                    </Col>
                    <Col xs={24} md={12} lg={8}>
                        <Card className="servive-card p-4" style={{ backgroundColor: "#F4C384" }}>
                            <img alt="Img error" className="bg" src={chemestrywhite} />
                            <img alt="Img error" className="mb-2" src={reception} />
                            <p className="title">Virtual qabulxona</p>
                        </Card>
                    </Col>
                    <Col xs={24} md={12} lg={8}>
                        <Card className="servive-card p-4" style={{ backgroundColor: "#CFCFD9" }}>
                            <img alt="Img error" className="bg" src={chemestrywhite} />
                            <img alt="Img error" className="mb-2" src={checklist} />
                            <p className="title">So'rvnoma</p>
                        </Card>
                    </Col>
                    <Col xs={24} md={12} lg={8}>
                        <Card className="servive-card p-4" style={{ backgroundColor: "#ECABA5" }}>
                            <img alt="Img error" className="bg" src={chemestrywhite} />
                            <img alt="Img error" className="mb-2" src={learning} />
                            <p className="title">Elektron kutubxona</p>
                        </Card>
                    </Col>
                    <Col xs={24} md={12} lg={8}>
                        <Card className="servive-card p-4" style={{ backgroundColor: "#D8D8D8" }}>
                            <img alt="Img error" className="bg" src={chemestrywhite} />
                            <img alt="Img error" className="mb-2" src={graduate} />
                            <p className="title">Abituriyentlar uchun</p>
                        </Card>
                    </Col>
                </Row>
            </div>
        </React.Fragment>
    );
}

export default Services;