import React from "react";
import { Menu, Button, Dropdown, Modal } from "antd";
import { withRouter } from "react-router-dom";

import {
    UserOutlined,
    LogoutOutlined,
    ExclamationCircleOutlined,
} from "@ant-design/icons";

const { confirm } = Modal;

const TopNavUserMenu = (props) => {

    function confirmLogOut() {
        confirm({
            title: "Do you wanna log out?",
            icon: <ExclamationCircleOutlined />,
            content: "Then you have to log in again, wa are not gonna save your login and password",
            okText: "Log out",
            cancelText: "Cancel",
            centered: true,
            onOk() {
                console.log('User logged out');
                window.location = "/login"
            },
            onCancel() {
                // console.log("Cancel");
            },
        });
    }

    return (
        <Dropdown
            trigger={["click"]}
            overlay={
                <Menu>
                    <Menu.Item key="child1">
                        <UserOutlined />
                        Profile
                    </Menu.Item>

                    <Menu.Divider />

                    <Menu.Item key="child2" onClick={confirmLogOut}>
                        <LogoutOutlined />
                        Log out
                    </Menu.Item>
                </Menu>
            }
        >
            <Button shape="circle">
                <UserOutlined />
            </Button>
        </Dropdown>

    )
}

export default withRouter(TopNavUserMenu);