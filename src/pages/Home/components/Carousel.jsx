import React from 'react';
import { Carousel, Card } from 'antd';

const HomeCarousel = () => {
    return (
        <Carousel autoplay effect="fade">
            <div>
                <Card className="carousel-caption" bordered={false}>
                    <p>Card content 1</p>
                </Card>
            </div>
            <div>
                <Card className="carousel-caption" bordered={false}>
                    <p>Card content 2</p>
                </Card>
            </div>
            <div>
                <Card className="carousel-caption" bordered={false}>
                    <p>Card content 3</p>
                </Card>
            </div>
            <div>
                <Card className="carousel-caption" bordered={false}>
                    <p>Card content 4</p>
                </Card>
            </div>
        </Carousel>
    );
}
export default HomeCarousel;