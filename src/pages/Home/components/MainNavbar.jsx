import React from 'react';
import { withRouter } from "react-router-dom";
import { Button, Menu, Dropdown, Row, Col } from 'antd';
import { PhoneOutlined, DownOutlined, MenuOutlined } from '@ant-design/icons';

import logo from "../../../assets/img/logo.png";

const MainNavbar = (props) => {
    return (
        <React.Fragment>
            <Row className="mb-3" justify="space-between" align="middle">
                <Col>
                    <Row gutter={8} justify="start" align="middle">
                        <Col>
                            <img alt="Img error" src={logo} />
                        </Col>
                        <Col>
                            <p className="logo-text">Toshkent <br /> Kimyo-Texnologiya <br />Instituti</p>
                        </Col>
                    </Row>
                </Col>
                <Col className="visible-lg">
                    <Row gutter={[8, 8]}>
                        <Col>
                            <Button key="1" type="text" className="phone-text" icon={<PhoneOutlined />}>+998 71 244-78-49</Button>
                        </Col>
                        <Col>
                            <Button key="3" type="primary" className="custom-btn covid">Covid-19</Button>
                        </Col>
                        <Col>
                            <Button key="2" className="custom-bordered-btn mail">Pochta</Button>
                        </Col>
                        <Col>
                            <Button key="4" className="custom-btn vurtual-reception" onClick={() => props.history.push('/virtual-reception')}>Virtual qabulxona</Button>
                        </Col>
                    </Row>
                </Col>
                <Col className="visible-xs visible-sm visible-md">
                    <Button onClick={props.showDrawer}>
                        <MenuOutlined />
                    </Button>
                </Col>
            </Row>

            <div className="visible-lg">
                <Dropdown overlay={commonMenu} trigger={['click']}>
                    <Button type="text" className="menu-label">
                        Umumiy ma'lumotlar <DownOutlined />
                    </Button>
                </Dropdown>

                <Dropdown overlay={structureMenu}>
                    <Button type="text" className="menu-label">
                        Tuzilma <DownOutlined />
                    </Button>
                </Dropdown>
            </div>
        </React.Fragment>
    );
}

export default withRouter(MainNavbar);


const commonMenu = (
    <Menu className="menu-content pb-2" >
        <Row>
            <Col sm={24} md={24} lg={12}>
                <Menu>
                    <Menu.Item key="1">
                        1st menu item
                    </Menu.Item>
                    <Menu.Item key="2">
                        Uzunroq menu nomi Uzunroq menu  nomi Uzunroq menu nomi
                    </Menu.Item>
                    <Menu.Item key="3">
                        3rd item
                    </Menu.Item>
                </Menu>
            </Col>
            <Col sm={24} md={24} lg={12}>
                <Menu>
                    <Menu.Item key="11">
                        1st menu item
                    </Menu.Item>
                    <Menu.Item key="22">
                        2nd menu item
                    </Menu.Item>
                    <Menu.Item key="33">
                        3rd item
                    </Menu.Item>
                </Menu>
            </Col>
        </Row>
    </Menu>
);

const structureMenu = (
    <Menu>
        <Menu.Item key="1">
            1st menu item
        </Menu.Item>
        <Menu.Item key="2">
            2nd menu item
        </Menu.Item>
        <Menu.Item key="3">
            3rd item
        </Menu.Item>
    </Menu>
);