import React from 'react';
import { Link } from 'react-router-dom';
import { Layout, Menu, Drawer } from 'antd';
import { withRouter } from "react-router-dom";

import {
    HomeOutlined,
    BarChartOutlined,
} from '@ant-design/icons';

const { Sider } = Layout;
const { SubMenu } = Menu;


const mainMenus = [
    {
        label: "Home",
        path: "/dashboard",
        icon: <HomeOutlined />,
        children: null
    },
    {
        label: "Info",
        path: "/dashboard/info",
        icon: <BarChartOutlined />,
        children: null,
    },
]

const Menus = (props) => (
    <Menu mode="inline" defaultSelectedKeys={[props.location.pathname]}>
        {
            mainMenus.map((menu) => (
                menu.children && Array.isArray(menu.children) ? (
                    <SubMenu
                        key={menu.path}
                        title={
                            <span>
                                {menu.icon}
                                <span>{menu.label}</span>
                            </span>
                        }
                    >
                        {
                            menu.children.map((childMenu) => (
                                <Menu.Item key={childMenu.path} onClick={() => this.goToMenu(childMenu.path)}>
                                    <Link to={childMenu.path}>
                                        {childMenu.icon}
                                        {childMenu.label}
                                    </Link>
                                </Menu.Item>
                            ))
                        }
                    </SubMenu>
                ) : (
                        <Menu.Item key={menu.path}>
                            <Link to={menu.path}>
                                {menu.icon}
                                <span>{menu.label}</span>
                            </Link>
                        </Menu.Item>
                    )
            ))
        }
    </Menu>
)

const Sidebar = (props) => {
    if (window.innerWidth > 1300) {
        return (
            <Sider trigger={null} theme='light' collapsible collapsed={props.collapsed}>
                <Menus {...props} />
            </Sider>
        )
    } else {
        return (
            <Drawer
                title="Menus"
                placement="left"
                closable={false}
                onClose={props.onClose}
                visible={!props.collapsed}
                bodyStyle={{
                    padding: 0
                }}
            >
                <Menus {...props} />
            </Drawer>
        )
    }

}

export default withRouter(Sidebar);