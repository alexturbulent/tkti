import React from 'react';
import { Row, Col } from 'antd';

const FooterSocialNetwork = () => {
    return (
        <Row>
            <Col sm={24} md={12} lg={9}>
                <p className="address">Toshkent sh. Navoiy ko’chasi, 32 uy, 100011</p>
                <p>Telefon: (998-71)244-79-20 Faks: (998-71)244-79-17 E-mail: info@tcti.uz, txti_rektor@edu.uz</p>
            </Col>
            <Col sm={24} md={12} lg={9}>
                <p>AT markazi 2017. Barcha huquqlar himoyalangan.</p>
            </Col>
        </Row>
    );
}

export default FooterSocialNetwork;