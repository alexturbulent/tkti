import React from 'react';
import { Row, Col, Steps } from "antd";
import logo from "../../assets/img/logo.png";

import ApplicationForm from "./components/ApplicationForm";
import Review from "./components/Review";

import "./reception.less";
import ApplyForm from './components/ApplyForm';

const { Step } = Steps;

export const steps = [
    {
        title: "Ariza topshirish",
    },
    {
        title: "Arizani ko'rib chiqish",
    },
    {
        title: "Yo'nalish tanlash",
    },
];

class VirtualReception extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showMilitary: false,
            showDisabled: false,
            showSertificates: false,
            current: 0,
            params: {
                first_name: null,
                last_name: null,
                patronymic_name: null,
                date_of_birth: null,
                gender: "man",
                photo_file: null,

                citizenship: null,
                city: null,
                region: null,
                address: null,
                nationality: null,
                passport_serial: null,
                passport_number: null,
                passport_by: null,
                passport_file: null,

                edu_level: null,
                edu_org_name: null,
                edu_org_city: null,
                edu_org_region: null,
                edu_org_type: null,
                edu_graduation_year: null,
                degree_serial: null,
                degree_number: null,
                degree_file: null,

                military_serial: null,
                military_number: null,
                military_unit: null,
                military_doc_date: null,
                military_doc_file: null,

                disabled_serial: null,
                disabled_number: null,
                disabled_group: null,
                disabled_doc_date: null,
                disabled_doc_file: null,

                sertificate_lang: null,
                sertificate_type: null,
                sertificate_level: null,
                sertificate_serial: null,
                sertificate_number: null,
                sertificate_doc_date: null,
                sertificate_doc_file: null,

                phone_number: null,
                email: null,
                additional_phone_number: null,


                education_type: null,
                education_lang: null,
                education_foreign_lang: null,
                education_faculty: null,
            }
        }
    }

    inputChangeHandler = (e) => {
        const name = e.target.name;
        const val = e.target.value;
        this.setState({
            params: {
                ...this.state.params,
                [name]: val,
            }
        })
    }

    dateChangeHandler = (value, dateString, name) => {
        this.setState({
            params: {
                ...this.state.params,
                [name]: dateString,
            }
        })
    }

    selectChangeHandler = (value, name) => {
        this.setState({
            params: {
                ...this.state.params,
                [name]: value,
            }
        })
    }

    normFile = (e) => {
        console.log('Upload event:', e);
        if (Array.isArray(e)) {
            // return e;
        }

        // return e && e.fileList;
    }

    switchChangeHandler = (checked, name) => this.setState({ [name]: checked });

    next = () => {
        const current = this.state.current + 1;
        this.setState({ current });
    }

    prev = () => {
        const current = this.state.current - 1;
        this.setState({ current });
    }

    applyApplication = () => {
        console.log('form params', this.state.params);
    }

    render() {
        const {
            showMilitary,
            showDisabled,
            showSertificates,
            current,
        } = this.state;

        return (
            <div className="wrap">
                <div className="top-nav container py-4 ">
                    <Row gutter={8} justify="start" align="middle">
                        <Col>
                            <img alt="Img error" src={logo} />
                        </Col>
                        <Col>
                            <p className="logo-text">Toshkent <br /> Kimyo-Texnologiya <br />Instituti</p>
                        </Col>
                    </Row>
                </div>

                <div className="application-form container">
                    <Steps current={current} className="mb-4">
                        {steps.map(item => (
                            <Step key={item.title} title={item.title} />
                        ))}
                    </Steps>

                    <div className="steps-content">
                        {
                            current === 0 && (
                                <ApplicationForm
                                    showMilitary={showMilitary}
                                    showDisabled={showDisabled}
                                    showSertificates={showSertificates}
                                    current={current}
                                    inputChangeHandler={this.inputChangeHandler}
                                    dateChangeHandler={this.dateChangeHandler}
                                    switchChangeHandler={this.switchChangeHandler}
                                    selectChangeHandler={this.selectChangeHandler}
                                    normFile={this.normFile}
                                    next={this.next}
                                    prev={this.prev}
                                    {...this.state.params}
                                />
                            )
                        }
                        {
                            current === 1 && (
                                <Review
                                    showMilitary={showMilitary}
                                    showDisabled={showDisabled}
                                    showSertificates={showSertificates}
                                    current={current}
                                    next={this.next}
                                    prev={this.prev}
                                    {...this.state.params}
                                />
                            )
                        }

                        {
                            current === 2 && (
                                <ApplyForm
                                    selectChangeHandler={this.selectChangeHandler}
                                    inputChangeHandler={this.inputChangeHandler}
                                    applyApplication={this.applyApplication}
                                    current={current}
                                    next={this.next}
                                    prev={this.prev}
                                    {...this.state.params}
                                />
                            )
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export default VirtualReception;